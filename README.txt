-- Description --

The Exclude Tags module adds an exclude tags filter to your site, for use with
text format. This filter adds the ability to remove specified tags in any
text field that uses a text format (such as the body of a content item or
the text of a comment).

By default this filter will remove <script>, <video>, <audio> tags
in any text field, If required user can add any number of tags to remove.

Installation
------------
1. Copy the entire  directory of exclude_tags to Drupal
sites/all/modules or sites/all/modules/contrib directory.

2. Login as an administrator to Enable the module in the
"Administer" -> Modules (admin/modules)

3. Access the links to add more tags to remove from text field
admin/config/content/formats/exclude_tags.

-- Author --
Arulraj
